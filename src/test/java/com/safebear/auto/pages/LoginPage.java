package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {
    LoginPageLocators locators = new LoginPageLocators();

    @NonNull
    WebDriver driver;

    public String getPageTitle() {
        return driver.getTitle();
    }

    public void login(String username, String password){
        driver.findElement(locators.getUsernamefield()).sendKeys(username);
        driver.findElement(locators.getPasswordfield()).sendKeys(password);
        driver.findElement(locators.getSubmitbutton()).click();
    }

    public String getfailedmessage(){
        return driver.findElement(locators.getValidationError()).getText();
    }
}
