package com.safebear.auto.tests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import cucumber.api.PendingException;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class StepDefs {
    LoginPage loginPage;
    ToolsPage toolsPage;
    WebDriver driver;

    @Before
    public void setUp() {
        driver = Utils.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);
    }


    @After
    public void tearDown() {
        driver.quit();
    }


    @Given("^I navigate to the login page$")
    public void i_navigate_to_the_login_page() throws Throwable {
        driver.get(Utils.getUrl());
        Assert.assertEquals("Login Page", loginPage.getPageTitle(), "Not login page");
    }

    @When("^I enter login info for a (.+)$")
    public void i_enter_login_info_for_a_invalidUser(String user) throws Throwable {
        switch (user) {
            case "invalidUser":
                loginPage.login("attacker", "pass");
                break;
            case "validUser":
                loginPage.login("tester", "letmein");
                break;
            default:
                Assert.fail("only use validUser or invalidUser");
                break;
        }
    }

    @Then("^I can see the following: (.+)$")
    public void i_can_see_the_following_u_p_incorrect(String message) throws Throwable {
        switch (message) {
            case "Username or Password is incorrect":
                Assert.assertTrue(loginPage.getfailedmessage().contains(message));
                break;
            case "Login Successful":
                Assert.assertTrue(toolsPage.loginsuccessmessage().contains(message));
                break;
            default:
                Assert.fail("Message data invalid");
                break;
        }
    }
}
