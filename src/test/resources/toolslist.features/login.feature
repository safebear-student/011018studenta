Feature: Login
  In order to access the website
  As a user
  I want to know if my login is successful

  Rules:
  * The user must be informed if login info is correct

  Glossary:
  User: Someone wants to create toolslist

  @HighRisk
  @HighImpact
  Scenario Outline: Nav and login
    Given I navigate to the login page
    When I enter login info for a <userType>
    Then I can see the following: <message>
    Examples:
      | userType    | message          |
      | invalidUser | Username or Password is incorrect    |
      | validUser   | Login Successful |
